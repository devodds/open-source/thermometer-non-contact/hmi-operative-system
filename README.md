[[_TOC_]]

# Overview
**Disclaimer**: This is a brief project description and not a Buildroot tutorial.

Linux Operative System built using https://buildroot.org/. As a starting point, we used `beaglebone_qt5_defconfig`

## Kernel
This project use kernel version 4.19.79, but it is not the mainline linux. It is the kernel modified by Texas Instrument to be used with beagleboards.

The [fragment](board/bbg/linux-sgx.fragment) file has the configurations changed, the rest of configurations are the default from kernel.

### Custom configs
Most of them are related with de DRM and the OpenGL use.

## File System
The file system was built considering the development of HMI applications using Qt5.

It includes:
- Qt5
- GStreamer and video libraries (includes SGX GPU driver)
- DHCP Client enabled
- SSH services
  - Dropbear SSH server with desabled reverse DNS
  - Green End SFTP Server

## Bootloader
U-Boot 

# Usage
In order to use the image generated you need:
- BeagleBone Green from Seeed Studio
- LCD Cape 4.3" from 4D Systems

## Dowload the image
1. Got to the last release and download `sdcard.img`
1. Write the image to a uSD card, for example, using `dd`
```sh
sudo dd if=output/images/sdcard.img of=/dev/sdb bs=1M
```
3. Boot it!
